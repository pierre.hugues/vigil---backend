FROM node:12-alpine

WORKDIR /usr/src/app

COPY . .

RUN mkdir /config \
    && rm /usr/src/app/config/config.json \
    && ln -s /config/config.json /usr/src/app/config/config.json \
    && ln -s /config/secret_config.json /usr/src/app/config/secret_config.json

VOLUME /config

RUN apk --no-cache --virtual build-dependencies add \
    python \
    make \
    g++ \
    && npm install --production \
    && apk del build-dependencies \
    && apk add nmap

EXPOSE 4000
CMD [ "node", "index.js" ]