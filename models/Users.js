const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
    name: String,
    mail: String,
    password: String,
    confirmed: {
        type: Boolean,
        default: false,
    },
    notif: {
        type: Boolean,
        default: false,
    },
    refreshRate: {
        type: Number,
        default: 120,
    },
}, {
    timestamps: true,
});

module.exports = mongoose.model("User", UserSchema);