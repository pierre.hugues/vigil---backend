const mongoose = require("mongoose");

const ServiceSchema = mongoose.Schema({
    name: String,
    address: String,
    port: Number,
    refreshPeriod: {
        type: Number,
        default: 120,
    },
    monitored: {
        type: Boolean,
        default: false,
    },
    status: {
        type: String,
        default: "ignored",
    },
    logs: [{
        date: {
            type: Date,
            default: new Date(),
        },
        ping: Number,
    }],
    lastRemind: {
        type: Date,
        default: null,
    },
}, {
    timestamps: true,
});

module.exports = mongoose.model("Service", ServiceSchema);