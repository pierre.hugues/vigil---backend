const axios = require("axios");

const url = "http://localhost:4000";

describe("Server status", () => {
	test("Server is online", () => {
		return axios.get(url).then((response) => {
			expect(response).toBeDefined();
			expect(response.status).toBe(200);
		});
	});
});
