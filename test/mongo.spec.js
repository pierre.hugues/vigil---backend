const MongoClient = require("mongodb").MongoClient;
const config = require("../config/config.json");

let host = process.env.DB_HOST || config && config.server && config.mongo.DB_HOST || "localhost";
let port = process.env.DB_PORT || config && config.server && config.mongo.DB_PORT || "27017";
let dbName = process.env.DB_DB_NAME || config && config.server && config.mongo.DB_NAME || "Vigil";
let collectionName = process.env.DB_COLLECTION || config && config.server && config.mongo.DB_COLLECTION || "Services";

// Connection URL
const url = "mongodb://" + host + ":" + port;

describe("MongoDB", () => {
	test("Connect to MongoDB", (done) => {
		MongoClient.connect(url, (err, client ) => {
			expect(err).toBeNull();
			client.close();
			done();
		});
	});

	test("Connect to Vigil Collection", (done) => {
		MongoClient.connect(url, (err, client ) => {
			expect(err).toBeNull();
			const db = client.db(dbName);
			const collection = db.collection(collectionName);
			expect(collection).toBeDefined();
			client.close();
			done();
		});
	});
});
