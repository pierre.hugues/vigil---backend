# Vigil - Backend

REST API Using Express to link Vigil application to a MongoDB database with authentification.

## Installation

### Prerequesites

You need **Node.JS** and **MongoDB** to install and use the application.

### Installation Procedure

In the **MongoDB Shell** or using a software linked to your MongoDB, create a database called ```vigil```.
You need to create a config file in `./config`container containing different configuration information for the server. It should have the corresponding structure :

```json
config.json
---

{
    "server": { //Config of the backend
        "SERVER_PROTOCOL": "http",
        "SERVER_HOSTNAME": "localhost",
        "SERVER_PORT": "4000"
    },
    "client": { //config of the frontend
        "SERVER_PROTOCOL": "http",
        "SERVER_HOSTNAME": "localhost",
        "SERVER_PORT": "8080" // If the client is running on a domain, do not put this line
    },
    "mongo": { //config of the database
        "DB_HOST": "localhost",
        "DB_PORT": "27017",
        "DB_NAME": "vigil",
    "DB_COLLECTION": "Services"
    }
}
```

```json
secret_config.json
---

{
    "mail": { //config of the mail adress to use to send mails
        "host": "<host>",
        "port": "<port>",
        "secure": "<true/false>",
        "auth": {
            "user": "<mail>",
            "pass": "<password>"
        },
        "tls": {
            "rejectUnauthorized": "<true/false>"
        }
    },
    "SECRET_KEY": "<random key>" // random key for JWT
}
```

## Usage

### Docker

A Docker container is available at pierrehugues/vigil-backend. It uses a volume called /config to set the configuration for the different parts of the application.

3 images are available :

- `:master`: latest successfull push on master
- `:latest`: latest successfull tag
- `:v.x.y.z`: specific version

Launch the Docker container using :

```shell
docker run -d --name vigil_backend --net=host --restart always -v "$(pwd)/config":/config pierrehugues/vigil-backend:latest
```

### Running the application

You can run the application using the `npm start` command linked to a process manager such as `pm2` if you don't want to use a Docker container.
