module.exports = function(router) {

    router.route("/").get((req, res) => {
        try {
            res.status(200).end();
        } catch (e) {
            console.error(e);
            res.status(500).send(e);
        }
    });

};