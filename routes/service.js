const jwt = require("jsonwebtoken");
const nmap = require("node-nmap");
nmap.nmapLocation = "nmap";

let secret_config;
try {
    secret_config = require("../config/secret_config.json");
} catch (error) {
    logger.error("Secret config missing");
}

let Service = require("../models/Services");

module.exports = function(router, monitor) {

    // Create a new service
    router.route("/service").post(async (req, res) => {
        try {
            let token = req.headers.authorization;
            if(!token) throw Error("No token provided");
            await new Promise((rs, rj) => {
                jwt.verify(token, secret_config.SECRET_KEY, (err, decoded) => {
                    if(err) rj(err);
                    else rs(decoded);
                });
            });
            let service = new Service(req.body);
            await service.save();
            if(service.monitored) {
                monitor.addPing(service);
            }
            res.status(200).json("Service successfully added.");
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error when saving to database : ${error}.`);
        }
    });

    // List all services
    router.route("/services").get(async (req, res) => {
        try {
            let token = req.headers.authorization;
            if(!token) throw Error("No token provided");
            await new Promise((rs, rj) => {
                jwt.verify(token, secret_config.SECRET_KEY, (err, decoded) => {
                    if(err) rj(err);
                    else rs(decoded);
                });
            });
            let services = await Service.find();
            res.status(200).json(services);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error getting the services : ${error}.`);
        }
    });

    // Get information from one service
    router.route("/services/:id").get(async (req, res) => {
        try {
            let token = req.headers.authorization;
            if(!token) throw Error("No token provided");
            await new Promise((rs, rj) => {
                jwt.verify(token, secret_config.SECRET_KEY, (err, decoded) => {
                    if(err) rj(err);
                    else rs(decoded);
                });
            });
            let service = await Service.findById(req.params.id);
            if(!service) throw Error("No service by this ID");
            res.status(200).json(service);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error getting the service : ${error}.`);
        }
    });

    // Update a service
    router.route("/services/:id").put(async (req, res) => {
        try {
            let token = req.headers.authorization;
            if(!token) throw Error("No token provided");
            await new Promise((rs, rj) => {
                jwt.verify(token, secret_config.SECRET_KEY, (err, decoded) => {
                    if(err) rj(err);
                    else rs(decoded);
                });
            });
            let service = await Service.findByIdAndUpdate(req.params.id, req.body, {
                new: true,
            });
            if(!service) throw Error("No service by this ID");
            if(service.monitored) {
                monitor.updatePing(service);
            } else {
                monitor.deletePing(service);
            }
            res.status(200).json("Service updated successfully.");
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error updating the service : ${error}.`);
        }
    });

    // Delete a service
    router.route("/services/:id").delete(async (req, res) => {
        try {
            let token = req.headers.authorization;
            if(!token) throw Error("No token provided");
            await new Promise((rs, rj) => {
                jwt.verify(token, secret_config.SECRET_KEY, (err, decoded) => {
                    if(err) rj(err);
                    else rs(decoded);
                });
            });
            let service = await Service.findByIdAndDelete(req.params.id);
            if(!service) throw Error("No service by this ID");
            monitor.deletePing(service);
            res.status(200).json("Service successfully removed.");
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error removing the service : ${error}.`);
        }
    });

    //  Import via CSV
    router.route("/services/csv").post(async (req, res) => {
        try {
            let token = req.headers.authorization;
            if(!token) throw Error("No token provided");
            await new Promise((rs, rj) => {
                jwt.verify(token, secret_config.SECRET_KEY, (err, decoded) => {
                    if(err) rj(err);
                    else rs(decoded);
                });
            });
            let data = req.body.csv.trim().split("\n");
            for(let line of data) {
                let info = line.split(";");
                let service = new Service({
                    name: info[0],
                    address: info[1],
                    port: info[2],
                    refreshPeriod: info[3],
                    monitored: JSON.parse(info[4]),
                });
                await service.save();
                if(service.monitored) {
                    monitor.addPing(service);
                }
            }
            res.status(200).json("Services successfully imported.");
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error importing the services : ${error}.`);
        }
    });

    //  Import via NMAP
    router.route("/services/nmap").post(async (req, res) => {
        try {
            let token = req.headers.authorization;
            if(!token) throw Error("No token provided");
            await new Promise((rs, rj) => {
                jwt.verify(token, secret_config.SECRET_KEY, (err, decoded) => {
                    if(err) rj(err);
                    else rs(decoded);
                });
            });
            let adress = req.body.adress;
            if(!adress) throw Error("No adress specified");
            let scan = new nmap.NmapScan(adress);

            scan.on("complete", async (data) => {
                try {
                    let ports = data[0].openPorts;
                    for(let port of ports) {
                        let service = new Service({
                            name: `${adress}:${port.port}`,
                            address: adress,
                            port: port.port,
                            monitored: true,
                        });
                        await service.save();
                        monitor.addPing(service);
                    }
                    res.status(200).json("Services successfully imported.");
                } catch (error) {
                    res.status(400).json(`Error importing the services : ${error}.`);
                }
            });

            scan.on("error", (error) => {
                res.status(400).json(`Error importing the services : ${error}.`);
            });

            scan.startScan();
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error importing the services : ${error}.`);
        }
    });
};