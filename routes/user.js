const bcrypt = require("bcrypt");
const fs = require("fs").promises;
const jwt = require("jsonwebtoken");
const mail = require("../lib/mail");
const config = require("../config/config.json");

let secret_config;
try {
    secret_config = require("../config/secret_config.json");
} catch (error) {
    logger.error("Secret config missing");
}

let User = require("../models/Users");

module.exports = function(router) {

    // Create new user
    router.route("/signup").post(async (req, res) => {
        try {
            req.body.password = await bcrypt.hash(req.body.password, 10);
            let user = new User(req.body);
            let data = await user.save();

            // Send verif mail
            let body = await fs.readFile(__dirname + "/../templates/verif.html", "utf8");
            if(config.client.SERVER_PORT) {
                body = body.replace("$LINK", `${config.client.SERVER_PROTOCOL}://${config.client.SERVER_HOSTNAME}:${config.client.SERVER_PORT}/verify/${user.id}`);
            } else {
                body = body.replace("$LINK", `${config.client.SERVER_PROTOCOL}://${config.client.SERVER_HOSTNAME}/verify/${user.id}`);
            }
            await mail.SendMail({
                from: secret_config.mail.auth.user,
                to: req.body.mail,
                subject: "Please confirm your mail",
                body: body,
            }, true);

            res.status(200).json(data);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error when saving the user : ${error}.`);
        }
    });

    // Verify a user
    router.route("/verif/:id").get(async (req, res) => {
        try {
            let data = await User.findByIdAndUpdate(req.params.id, {confirmed: true});
            res.status(200).json(data);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error when verifying the user : ${error}.`);
        }
    });

    // List all users
    router.route("/users").get(async (req, res) => {
        try {
            let users = await User.find();
            res.status(200).json(users);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error getting the users : ${error}.`);
        }
    });

    // Resend a confimation mail to the adress of a user
    router.route("/resendMail/:id").get(async (req, res) => {
        try {
            let user = await User.findById(req.params.id);
            if(!user) throw Error("No user found.");
            // Send verif mail again
            let body = await fs.readFile(__dirname + "/../templates/verif.html", "utf8");
            if(config.client.SERVER_PORT) {
                body = body.replace("$LINK", `${config.client.SERVER_PROTOCOL}://${config.client.SERVER_HOSTNAME}:${config.client.SERVER_PORT}/verify/${req.params.id}`);
            } else {
                body = body.replace("$LINK", `${config.client.SERVER_PROTOCOL}://${config.client.SERVER_HOSTNAME}/verify/${req.params.id}`);
            }
            await mail.SendMail({
                from: secret_config.mail.auth.user,
                to: user.mail,
                subject: "Please confirm your mail",
                body: body,
            }, true);
            res.status(200).json(user);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error resending the mail : ${error}.`);
        }
    });

    // Log a user
    router.route("/login").post(async (req, res) => {
        try {
            let user = await User.findOne({
                $or: [
                    {name: req.body.name},
                    {mail: req.body.name},
                ],
                confirmed: true,
            });
            if(!user) throw Error("No user found.");
            let pass = await bcrypt.compare(req.body.password, user.password);
            if(!pass) throw Error("No user found.");
            let token = jwt.sign(JSON.parse(JSON.stringify(user)), secret_config.SECRET_KEY, {
                expiresIn: 3600*6,
            });
            res.status(200).json({token: token, user: user});
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error logging the user : ${error}.`);
        }
    });

    // Check if the token is leggit
    router.route("/token/:token").get(async (req, res) => {
        try {
            logger.info("Checking token...");
            let token = req.params.token;
            await new Promise((rs, rj) => {
                jwt.verify(token, secret_config.SECRET_KEY, (err, decoded) => {
                    if(err) rj(err);
                    else rs(decoded);
                });
            });
            logger.info("Token verified...");
            res.status(200).json(token);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error verifying the token : ${error}.`);
        }
    });

    // Update a user
    router.route("/user/:id").put(async (req, res) => {
        try {
            let token = req.headers.authorization;
            if(!token) throw Error("No token provided");
            await new Promise((rs, rj) => {
                jwt.verify(token, secret_config.SECRET_KEY, (err, decoded) => {
                    if(err) rj(err);
                    else rs(decoded);
                });
            });
            if(req.body.password && req.body.oldPassword) {
                let user = await User.findById(req.params.id);
                if(!user) throw Error("No user found.");
                let pass = await bcrypt.compare(req.body.oldPassword, user.password);
                if(!pass) throw Error("No user found.");
                req.body.password = await bcrypt.hash(req.body.password, 10);
                delete req.body.oldPassword;
            }
            let user = await User.findByIdAndUpdate(req.params.id, req.body, {
                new: true,
            });
            if(!user) throw Error("No user by this ID");
            res.status(200).json(user);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error when updating the user : ${error}.`);
        }
    });

    // Send a link to reset a password
    router.route("/forgottenPwd").post(async (req, res) => {
        try {
            let token = req.headers.authorization;
            if(!token) throw Error("No token provided");
            await new Promise((rs, rj) => {
                jwt.verify(token, secret_config.SECRET_KEY, (err, decoded) => {
                    if(err) rj(err);
                    else rs(decoded);
                });
            });
            let user = await User.findOne({
                mail: req.body.mail,
                confirmed: true,
            });
            if(!user) throw Error("No user found.");
            res.status(200).json(user);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error when send the recuperation link : ${error}.`);
        }
    });

};