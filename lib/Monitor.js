const ping = require("tcp-ping");
const mail = require("./mail");

let Service = require("../models/Services");
let User = require("../models/Users");

class Monitor {

    /**
     * Constructor for the Monitor
     */
    constructor() {
        this.services = [];
        this.pings = {};
    }

    /**
     * Initialise the monitoring
     */
    async init() {
        this.services = await Service.find();
        for(let service of this.services) {
            if(service.monitored) {
                await this.addPing(service);
            }
        }
    }

    /**
     * Start the monitoring of a service
     * @param {Object} service Service to monitor
     */
    async addPing(service) {
        await this.startPing(service);
        this.pings[service.id] = setInterval(() => this.startPing(service), service.refreshPeriod * 1000);
    }

    /**
     * Ping a service
     * @param {Object} service Service to ping
     */
    async startPing(service) {
        try {
            // eslint-disable-next-line no-async-promise-executor
            await new Promise(async (resolve, reject) => {
                try {
                    ping.ping({
                        address: service.address,
                        port: service.port,
                        attempts: 4,
                    }, async (err, data) => {
                        if(err || isNaN(data.avg)) {
                            reject(err || data);
                        } else {
                            if(service.status === "down") {
                                service.lastRemind = null;
                                this.sendMailUp(service, data);
                            }
                            pingLogger.debug(data);
                            service.status = "up";
                            service.logs.push({
                                date: new Date(),
                                ping: data.avg,
                            });
                        }
                        resolve();
                    });
                } catch (error) {
                    reject(error);
                }
            });
            await service.save();
        } catch (error) {
            if(service.status === "up" || !service.lastRemind) {
                service.lastRemind = new Date();
                this.sendMailDown(service, error);
            } else if((service.lastRemind.getTime() + 1000 * 60 * 60 * 24) < new Date().getTime()) {
                this.sendMailDownReminder(service, error);
                service.lastRemind = new Date();
            }
            service.status = "down";
            service.logs.push({
                date: new Date(),
                ping: null,
            });
            pingLogger.error(error);
            await service.save();
        }
    }

    /**
     * Stop monitoring a service
     * @param {Object} service Service to no longer monitor
     */
    deletePing(service) {
        clearInterval(this.pings[service.id]);
        delete this.pings[service.id];
    }

    /**
     * Update the monitoring of a service
     * @param {Object} service Service to update
     */
    async updatePing(service) {
        this.deletePing(service);
        await this.addPing(service);
    }

    /**
     * Send a mail to the administrators to notify them of a service down
     * @param {Object} service Service down
     * @param {Error} error Error raised
     */
    async sendMailDown(service, error) {
        try {
            let users = await User.find();
            for(let user of users) {
                if(!user.notif) continue;
                await mail.SendMail({
                    to: user.mail,
                    subject: `DOWN : ${service.name} - ${service.address}:${service.port}`,
                    body: `Service DOWN\n[${new Date()}] ${JSON.stringify(error)}`,
                }, false);
            }
        } catch (error) {
            console.log(error);
        }
    }

    /**
     * Send a mail to the administrators to remind them of a service down
     * @param {Object} service Service down
     * @param {Error} error Error raised
     */
    async sendMailDownReminder(service, error) {
        try {
            let users = await User.find();
            for(let user of users) {
                if(!user.notif) continue;
                await mail.SendMail({
                    to: user.mail,
                    subject: `REMINDER DOWN : ${service.name} - ${service.address}:${service.port}`,
                    body: `Reminder service DOWN\n[${new Date()}] ${JSON.stringify(error)}`,
                }, false);
            }
        } catch (error) {
            console.log(error);
        }
    }

    /**
     * Send a mail to the administrators to notify them of a service up
     * @param {Object} service Service down
     * @param {Object} data Ping values
     */
    async sendMailUp(service, data) {
        try {
            let users = await User.find();
            for(let user of users) {
                if(!user.notif) continue;
                await mail.SendMail({
                    to: user.mail,
                    subject: `UP : ${service.name} - ${service.address}:${service.port}`,
                    body: `Service UP\n[${new Date()}] ${JSON.stringify(data)}`,
                }, false);
            }
        } catch (error) {
            console.log(error);
        }
    }
}

module.exports = Monitor;