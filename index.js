const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const log4js = require("log4js");
const mongoose = require("mongoose");

const config = require("./config/config.json");

// Configure the loggers
log4js.configure("./config/log4js.json");
global.logger = log4js.getLogger();
global.pingLogger = log4js.getLogger("ping");

const Monitor = require("./lib/Monitor.js");
const monitor = new Monitor();

const app = express();

// Configure the roads
const router = express.Router();
require("./routes/service")(router, monitor);
require("./routes/user")(router);
require("./routes/default")(router);

app.use(cors());
app.use(bodyParser.json());
app.use("/", router);

const port = config && config.server && config.server.SERVER_PORT || 4000;
app.listen(port, async () => {
    logger.info(`HTTP server listening on port ${port}`);
    // Connect to the database
    try {
        await mongoose.connect(`mongodb://${config.mongo.DB_HOST}:${config.mongo.DB_PORT}/${config.mongo.DB_NAME}`, {
            useNewUrlParser: true, 
            useUnifiedTopology: true,
            useFindAndModify: false,
        });
        logger.info("Database connection is successful");
        logger.info("Getting the services and start monitoring");
        await monitor.init();
        logger.info("Monitoring started");
    } catch (e) {
        logger.error(`Error when starting the server ${e}`);
    }
});
